﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AxisZRotation : MonoBehaviour {
    public float speed = 1.0f;
    float _rotation = 0.0f;
	
	// Update is called once per frame
	void Update () {
        _rotation += speed * Time.deltaTime;
        transform.rotation = Quaternion.Euler(0, 0, _rotation);
	}
}
