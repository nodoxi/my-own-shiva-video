﻿using UnityEngine;
using PathologicalGames;

public class FlyController : MonoBehaviour {
    float _speed = -2.0f;
	// Use this for initialization
	
    public void SetSpeed(float speed) {
        _speed = speed;
    }

	// Update is called once per frame
	void Update () {
        transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + _speed * Time.deltaTime);
        if (transform.position.z < -10f || transform.position.z > 100f) {
            PoolManager.Pools["objects"].Despawn(transform);
        }
	}
}
