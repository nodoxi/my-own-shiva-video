﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathologicalGames;
using Kino;

public class VideoController : MonoBehaviour {
    enum Mode { Shiva, Skeleton, Particle };
    Mode _mode = Mode.Particle;
    float _modeTimer = 0f;
    float _kaleidoscopeTimer = 10f;
    // Use this for initialization

    public Transform[] shivaPrefabs;
    public Transform[] skeletonPrefabs;
    public Transform[] particlePrefabs;
    Mirror _mirror;

    void Start () {
        _mirror = gameObject.GetComponentInChildren<Mirror>();
    }
	
	// Update is called once per frame
	void Update () {
        _modeTimer -= Time.deltaTime;
        if (_modeTimer < 0) {
            ChangeMode();
            _modeTimer = 2f;
        }

        _kaleidoscopeTimer -= Time.deltaTime;
        if  (_kaleidoscopeTimer < 0) {
            _mirror.SetRepeat(UnityEngine.Random.Range(1, 3)*2);
            _kaleidoscopeTimer = 10f;
        }

        
	}

    void ChangeMode() {
        Mode _newMode = (Mode)UnityEngine.Random.Range(0, Enum.GetNames(typeof(Mode)).Length);
        while (_newMode == _mode) {
            _newMode = (Mode)UnityEngine.Random.Range(0, Enum.GetNames(typeof(Mode)).Length);
        }
        _mode = _newMode;

        Transform _object;
        switch (_mode) {
            case Mode.Shiva:
                _object = PoolManager.Pools["objects"].Spawn(shivaPrefabs[(int)UnityEngine.Random.Range(0, shivaPrefabs.Length)], new Vector3(0, 0, 20f), Quaternion.Euler(0, 0, 0));
                _object.GetComponent<FlyController>().SetSpeed(UnityEngine.Random.Range(-3f, -1f));
                break;
            case Mode.Skeleton:
                _object = PoolManager.Pools["objects"].Spawn(skeletonPrefabs[(int)UnityEngine.Random.Range(0, skeletonPrefabs.Length)], new Vector3(0, 0, 20f), Quaternion.Euler(0, 0, UnityEngine.Random.Range(-30f, 30f)));
                _object.GetComponent<FlyController>().SetSpeed(UnityEngine.Random.Range(-2f, -1f));
                break;
            case Mode.Particle:
                _object = PoolManager.Pools["objects"].Spawn(particlePrefabs[(int)UnityEngine.Random.Range(0, particlePrefabs.Length)], new Vector3(0, 0, 20f), Quaternion.Euler(0, 0, 0));
                _object.GetComponent<FlyController>().SetSpeed(-2f);
                ChangeMode();
                break;
        }
    }
}
